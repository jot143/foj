import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http, HttpOptions } from '@capacitor-community/http';
import { Platform } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { from } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class RestApiService {



    constructor(public platform: Platform, public http: HttpClient) {

    }

    get(url) {

        const token = localStorage.getItem('token');

        if (this.platform.is('capacitor')) {
            let headers: any = {
                'Accept': '*/*',
                'Content-Type': 'application/json'
            };
            if(token != null) {
                headers.Authorization = 'Bearer ' + token;
            }
            
            return from(Http.get({
                url: environment.url + url,
                headers
            }));
        } else {
            let headers = new HttpHeaders().set('Accept', '*/*').set('Content-Type', 'application/json');
            if(token != null) {
                headers.set('Authorization', 'Bearer ' + token);
            }
            return this.http.get(environment.url + url, {headers});
        }
    }

    post(url, data = {}) {

        const token = localStorage.getItem('token');

        if (this.platform.is('capacitor')) {
            let headers:any = {
                'Accept': '*/*',
                'Content-Type': 'application/json'
            };
            if(token != null) {
                headers.Authorization = 'Bearer ' + token;
            }

            return from(Http.post({
                url: environment.url + url,
                data,
                headers
            }));
        } else {

            let headers = new HttpHeaders().set('Accept', '*/*').set('Content-Type', 'application/json');
            if(token != null) {
                headers.set('Authorization', 'Bearer ' + token);
            }
            return this.http.post(environment.url + url, data, {headers});
        }
    }
}
