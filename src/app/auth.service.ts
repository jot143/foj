import { Injectable } from '@angular/core';
// import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { RestApiService } from './rest-api.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  text: any;
  userProfile;
  userData;
  dataOfTrackCaseWithId;
  lpcDetails;
  nemsInfo;
  constructor(private http: RestApiService) { }


  //   loginAdmin(body: any) {
  //     let headers = new HttpHeaders();
  //   this.http1.setDataSerializer('json');
  //  return this.http1.post('PensionRevision/ValidateUserLogin', body, {  }).then(
  //     res=>{
  //    return ((JSON.parse(res.data)));
  //     }
  //   )
  // }



  loginAdmin(body: any): Observable<any> {
    return this.http.post('PensionRevision/ValidateUserLogin', body).pipe(
      tap((res) => {
        localStorage.setItem('token', res.token);
      }),
    );
  }

 

  // getUserInfo(body){
  //   let token =localStorage.getItem('token')
  // const httpHeader = {
  //   'Content-Type':  'application/json',
  //   'Authorization': 'Bearer' + ' ' + token
  // };
  //   this.http1.setDataSerializer('json');
  //   return this.http1.post('PensionRevision/GetUserInfo', body, httpHeader).then(
  //     res=>{
  //       console.log("mymsg")

  //      return ((JSON.parse(res.data)));

  //     }
  //   )
  // }

  getUserInfo(body: any): Observable<any> {
    return this.http.post('PensionRevision/GetUserInfo', body);
  }
  // getDakStatus(body){
  //   let token =localStorage.getItem('token')
  // const httpHeader = {
  //   'Content-Type':  'application/json',
  //   'Authorization': 'Bearer' + ' ' + token
  // };
  //   this.http1.setDataSerializer('json');
  //   return this.http1.post('UserActivity/CheckDaKStatus', body, httpHeader).then(
  //     res=>{
  //     // this.userProfile= res;
  //      return ((JSON.parse(res.data)));
  //     }
  //   )
  // }

  getDakStatus(body): Observable<any> {
    return this.http.post('UserActivity/CheckDaKStatus', body);
  }
  // loginWithMpin(body){
  //   let token =localStorage.getItem('token')
  // const httpHeader = {
  //   'Content-Type':  'application/json',
  //   'Authorization': 'Bearer' + ' ' + token
  // };
  //   this.http1.setDataSerializer('json');
  //   return this.http1.post('PensionRevision/ValidateUserMPIN', body, httpHeader).then(
  //     res=>{
  //     // this.userProfile= res;
  //      return ((JSON.parse(res.data)));
  //       //console.log(JSON.stringify(res.data));
  //     }
  //   )
  // }

  loginWithMpin(body): Observable<any> {
    return this.http.post('PensionRevision/ValidateUserMPIN', body);
  }
  // getGrievanceList(){
  //   let token =localStorage.getItem('token')
  // const httpHeader = {
  //   'Content-Type':  'application/json',
  //   'Authorization': 'Bearer' + ' ' + token
  // };

  //   this.http1.setDataSerializer('json');
  //   return this.http1.get('Master/GetGrievanceSubjects', {}, httpHeader).then(
  //     res=>{
  //     // this.userProfile= res;
  //      return ((JSON.parse(res.data)));
  //       //console.log(JSON.stringify(res.data));
  //     }
  //   )
  // }

  getGrievanceList(): Observable<any> {
    return this.http.get('Master/GetGrievanceSubjects');
  }
  // createMpin(body){
  //   let token =localStorage.getItem('token')
  //   const httpHeader = {
  //     'Content-Type':  'application/json',
  //     'Authorization': 'Bearer' + ' ' + token
  //   };
  //     this.http1.setDataSerializer('json');
  //     return this.http1.post('PensionRevision/CreateUserMPIN', body, httpHeader).then(
  //       res=>{
  //       // this.userProfile= res;
  //        return ((JSON.parse(res.data)));
  //         //console.log(JSON.stringify(res.data));
  //       }
  //     )
  // }

  createMpin(body): Observable<any> {
    return this.http.post('PensionRevision/CreateUserMPIN', body);
  }
  // register(body){
  //   let token =localStorage.getItem('token')
  //   const httpHeader = {
  //     'Content-Type':  'application/json',
  //     'Authorization': 'Bearer' + ' ' + token
  //   };
  //     this.http1.setDataSerializer('json');
  //     return this.http1.post('PensionRevision/UserRegistration', body, {}).then(
  //       res=>{
  //       // this.userProfile= res;
  //        return ((JSON.parse(res.data)));
  //         //console.log(JSON.stringify(res.data));
  //       }
  //     )
  // }

  register(body): Observable<any> {
    return this.http.post('PensionRevision/UserRegistration', body);
  }

  changeUserMpin(body): Observable<any> {
    return this.http.post('PensionRevision/ChangeUserMPIN', body);
  }

  changePassword(body): Observable<any> {
    return this.http.post('PensionRevision/ChangeUserPassword', body);
  }

  getState(): Observable<any> {
    return this.http.get('Master/GetStateList');
  }

  getDistrict(val): Observable<any> {
    return this.http.get('Master/GetDistrictList?stateId=' + val);
  }

  getHolidaysList(): Observable<any> {
    return this.http.get('Master/GetHolidays');
  }

  getRelationsType(): Observable<any> {
    return this.http.get('Master/GetRelationships');
  }

  getFamilyPensionerType(): Observable<any> {
    return this.http.get('Master/GetFamilyPensionerType');
  }

  scheduleAppointment(body): Observable<any> {
    return this.http.post('Appointment/CreateNewAppointment', body);
  }

  getScheduleList(body): Observable<any> {
    return this.http.post('Appointment/GetAppointmentHistory', body);
  }

  cancelAppointment(body): Observable<any> {
    return this.http.post('Appointment/CancelAppointment', body);
  }

  rescheduleAppointment(body): Observable<any> {
    return this.http.post('Appointment/CreateNewAppointment', body);
  }

  sendFeedback(body): Observable<any> {
    return this.http.post('UserActivity/InsertNewFeedback', body);
  }
  s
  getfeedbackList(): Observable<any> {
    return this.http.post('UserActivity/GetFeedbackList', {});
  }

  getOtpForForgotPass(body): Observable<any> {
    return this.http.post('PensionRevision/CheckForgotPassWord', body);
  }

  changeForgotPass(body): Observable<any> {
    return this.http.post('PensionRevision/ResetPassWord', body);
  }

  newGrievance(body): Observable<any> {
    return this.http.post('Grievance/AddNewGrievance', body);
  }

  getOtpForRegister(body): Observable<any> {
    return this.http.post('PensionRevision/CheckUserDuplication', body);
  }

  cpcStatus(body): Observable<any> {
    return this.http.post('UserActivity/Check7CPCEppoStatus', body);
  }

  exGratiaEppoStatus(body): Observable<any> {
    return this.http.post('UserActivity/CheckExGratiaEppoStatus', body);
  }

  updateAddress(body): Observable<any> {
    return this.http.post('UserActivity/UpdatedAddressInfo', body);
  }

  getUpdateAddress(body): Observable<any> {
    return this.http.post('UserActivity/GetUpdatedAddressInfo', body);
  }

  getGrievanceStatus(body): Observable<any> {
    return this.http.post('Grievance/GetGrievanceDetails', body);
  }

  editProfile(body): Observable<any> {
    return this.http.post('UserActivity/UpdateBasicDetail', body);
  }

  getdownloadDoc(): Observable<any> {
    return this.http.post('UserActivity/GetPreLoginDocuments', {});
  }

  getdownloadDocAfterLogin(): Observable<any> {
    return this.http.post('UserActivity/GetPostLoginDocuments', {});
  }
  
  getDownloadsDocAfterLoginUserSpecific(body): Observable<any> {
    return this.http.post('UserActivity/GetDownloadFiles', body);
  }

  viewnems(body): Observable<any> {
    return this.http.post('UserActivity/GetNemsBCORPC', body);
  }

  getLpcDetails(body): Observable<any> {
    return this.http.post('UserActivity/GetLPCDetail', body);
  }

  trackCaseWithId(body): Observable<any> {
    return this.http.post('Grievance/GetGrievanceTrackDetails', body);
  }

  getOtpForMailChange(body): Observable<any> {
    return this.http.post('UserActivity/SendOTPForChangeEmail', body);
  }

  getOtpForMobileChange(body): Observable<any> {
    return this.http.post('UserActivity/SendOTPForChangeMobileNo', body);
  }

  changeMail(body): Observable<any> {
    return this.http.post('UserActivity/ChangeEmail', body);
  }

  changeMobileNo(body): Observable<any> {
    return this.http.post('UserActivity/ChangeMobileNo', body);
  }

  editLpcProfile(body): Observable<any> {
    return this.http.post('UserActivity/InsertUpdateLPC', body);
  }

  uploadDoc(body): Observable<any> {
    return this.http.post('UserActivity/UploadDocumentFile', body);
  }
}
