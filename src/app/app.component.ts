import { Component, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, IonRouterOutlet, ModalController, Platform } from '@ionic/angular';
import { SplashComponent } from './pages/splash/splash.component';
import { Location } from '@angular/common';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  
    // navigate =
    // [
    //   {
    //     title : "Home",
    //     url   : "/doctorlist",
    //     icon  : "people-outline"
    //   },
    //   {
    //     title : "Downloads",
    //     url   : "/doctorlist",
    //     icon  : "people-outline"
    //   },
    //   {
    //     title : "User Manual",
    //     url   : "/doctorlist",
    //     icon  : "people-outline"
    //   },
    //   {
    //     title : "FAQ",
    //     url   : "/dashboard",
    //     icon  : "podium-outline"
    //   },
    //   {
    //     title : "What's New",
    //     url   : "/dashboard",
    //     icon  : "call-outline"
    //   },
    //   {
    //     title : "External Links",
    //     url   : "/login",
    //     icon  : "call-outline"
    //   },
    //   {
    //     title : "More",
    //     url   : "/register",
    //     icon  : "call-outline"
    //   },
    // ]

    @ViewChild(IonRouterOutlet, {static: true}) routeroutlet: IonRouterOutlet;
    @ViewChildren(IonRouterOutlet) routerOutlets: QueryList < IonRouterOutlet > ;
    lastTimeBackPress = 0;
    timePeriodToExit = 2000;
    navigate : any;
  constructor( public modalController: ModalController,private platform: Platform,private alertController:AlertController,  private router : Router, private location: Location,
    ) {
    platform.ready().then(() => {
      this.backButtonEvent();
      // if (platform.is('cordova')) {

      //   platform.backButton.subscribeWithPriority(10, () => {
      //     //navigator['app'].exitApp();
      //   });
      //   console.log('cordova');


      // }
  });
  }
  
  async openAlert(){
    const alert=await this.alertController.create({
      header: 'Check Network Connection',
      message: 'You do not have Internet Connection',
      buttons:[{
        text:'ok',
        handler:()=>{
          navigator['app'].exitApp();
        }
      }]
    });
    await alert.present();
      }
  backButtonEvent() {
    this.platform.backButton.subscribeWithPriority(0, () => {
      this.routerOutlets.forEach(async(outlet: IonRouterOutlet) => {
        if (this.router.url != '/home') {
          // await this.router.navigate(['/']);
          await this.location.back();
        } else if (this.router.url === '/home') {
          if (new Date().getTime() - this.lastTimeBackPress >= this.timePeriodToExit) {
            this.lastTimeBackPress = new Date().getTime();
            this.presentAlertConfirm();
          } else {
            navigator['app'].exitApp();
          }
        }
      });
    });
  }
  
  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: 'Rodra Confirmation',
      message: 'Are you sure you want to exit the Rodra App?',
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
        cssClass: 'secondary',
        handler: (blah) => {}
      }, {
        text: 'Close App',
        handler: () => {
         // this.offlineStatus();
          navigator['app'].exitApp();
        }
      }]
    });
  
    await alert.present();
  }
}
