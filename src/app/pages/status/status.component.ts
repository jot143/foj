import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth.service';

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.scss'],
})
export class StatusComponent implements OnInit {
  username: any;
  username1: any;
  randomNumber: any;
  randomNumberEntered: any;

  constructor(private auth:AuthService) { }
   toShow=true;
  ngOnInit() {
    //this.randomNumber=
    this.generateRandomNumber(1111,9999);}
  show(ev){
    let val=ev.target.value;
   if(val=="status"){
    this.toShow=true;
   }
   else{
    this.toShow=false;
   }
  }
  generateRandomNumber(min,max){
    var rand=min+Math.random()*(max-min);
        rand=Math.round(rand);
        this.randomNumber= rand.toString();
  }
  getUserName(val){
    if(val==1){
    this.username=this.username.toUpperCase()
    }
    else{
      this.username1=this.username1.toUpperCase()
    }
  }
  submit(){
    this.username= this.username.toUpperCase()
    if(this.validateUserID(this.username)){
    if(this.randomNumber !=this.randomNumberEntered){
      this.randomNumberEntered="";
      //this.randomNumber=
      this.generateRandomNumber(1111,9999);
      alert("Enter Correct Captcha")
    }
    else{
      let body={
        "perno":this.username
    }
    this.auth.cpcStatus(body).subscribe(res=>
      {
     if(res.Status==1){
       alert(res.Message)
       this.generateRandomNumber(1111,9999);
     }
     else{
      this.generateRandomNumber(1111,9999);
     }
      })
    }
  }
  }
  exGratiaEppoStatus(){
    this.username1= this.username1.toUpperCase()
    if(this.validateUserID(this.username1)){
    let body={
      "perno":this.username1
  }
    this.auth.exGratiaEppoStatus(body).subscribe(res=>
      {
     if(res.Status){
       alert(res.Message)
     }
      })
    }
    }
    validateUserID(username){
      let regex= "^[A-Z0-9]*$"
       if( username.match(regex))
       {
         return true;
       }
       else
       {
         alert("Space and Special Charcters not allowed");
         return false;
         
       }
     }
}
