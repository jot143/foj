import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActionSheetController, ModalController, PopoverController } from '@ionic/angular';
import { AuthService } from 'src/app/auth.service';
import { EditlpcComponent } from '../editlpc/editlpc.component';
import { LpcinvoiceComponent } from '../lpcinvoice/lpcinvoice.component';
import { PersonalMenuPopComponent } from '../personal-menu-pop/personal-menu-pop.component';
import { UploaddataComponent } from '../uploaddata/uploaddata.component';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { DatePicker } from '@ionic-native/date-picker/ngx';
@Component({
  selector: 'app-openlpc',
  templateUrl: './openlpc.component.html',
  styleUrls: ['./openlpc.component.scss'],
})
export class OpenlpcComponent implements OnInit {
  @Input() editData;
  cda='ashs';
  body;
  relationsType;
  familyPensionerType;
  relation;
  pensionerType;
  uploadText:any;
  downloadText:any;
  fileTransfer:FileTransferObject;
  bsrcode;accountno;ifsccode;payingaddress;payingcity;pincode;bankaddress;bsrcodepaying;
  spouse_aadhar;spouse_dob;spouseemail;spousemobile;spouserelation;spousetype;spouse_PAN;
  toCompare: Date;
  today = new Date();
  fileName: string;
  fileType: string;
  constructor(private modalCtrl: ModalController,private popoverController:PopoverController,private router:Router,private datePicker: DatePicker,
    public actionSheetController: ActionSheetController,private auth:AuthService,
    private transfer: FileTransfer,
    private file: File,
    private filepath: FilePath,
    private filechooser: FileChooser) {
      this.uploadText='';
      this.downloadText="";
      this.getRelations();
      this.getFamilyPensionerType()
     }

  ngOnInit() {
    this.body=this.auth.lpcDetails
    console.log("cggklj")
    this.relation=this.body.Relation
    this.checkPensionerType();
    //this.pensionerType=this.body.Pensioner_Type
    console.log(this.relation);
    console.log(this.pensionerType);
  }
  getRelations(){
  this.auth.getRelationsType().subscribe(res=>{
    this.relationsType=res;
    console.log(JSON.stringify(res))
  })
  }
getFamilyPensionerType(){
  this.auth.getFamilyPensionerType().subscribe(res=>{
    this.familyPensionerType=res;
    console.log(JSON.stringify(res))
  })
}
async uploadlpc(){
  const modal = await this.modalCtrl.create({
    component: UploaddataComponent,
    componentProps: {

    }

  });
  return await modal.present();
}
 
checkPensionerType(){
  if(this.body.Pensioner_Type=="NOK"){
this.pensionerType=3
  }
  else if(this.body.Pensioner_Type=="Offr"){
this.pensionerType=1
  }
  else if(this.body.Pensioner_Type=="Spouse"){
this.pensionerType=2
  }
}

async invoicelpc(){
  const modal = await this.modalCtrl.create({
    component: LpcinvoiceComponent,
    componentProps: {
      editData:this.body
    }

  });
  return await modal.present();
}
selectRelations(event){
  this.relation=event.target.value
  
}
selectFamilyPensionerType(event){
  this.pensionerType=event.target.value
  
}
async editBasic(){
  this.auth.userData=this.body
  this.router.navigate(['/editLpcProfile'])
}
async invoiceOpen(){
  const modal = await this.modalCtrl.create({
    component: LpcinvoiceComponent,
    componentProps: {
    editData:this.body
    }

  });
  return await modal.present();
}
basicHome(){
  this.modalCtrl.dismiss();
}

uploadDoc(){
  this.auth.userData=this.body
  this.uploadFile()
  //this.router.navigate(['/uploadDoc'])
}

showDatepicker(value){
if(value==1){
  this.datePicker.show({
    date: new Date(),
    mode: 'date',
    androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK,
    okText:"Save Date",
    todayText:"Set Today"
  }).then(
    date => {
      this.toCompare=new Date(date.getFullYear(), date.getMonth(), date.getDate())
      if(this.today>this.toCompare){
        this.body.NOK_DOB = date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear();
    }
    else{
      alert("Invalid Date")
    }
  },
    err => console.log('Error occurred while getting date: ', err)
  );
}
else if(value==2){
  this.datePicker.show({
    date: new Date(),
    mode: 'date',
    androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK,
    okText:"Save Date",
    todayText:"Set Today"
  }).then(
    date => {
      this.toCompare=new Date(date.getFullYear(), date.getMonth(), date.getDate())
      if(this.today>this.toCompare){
        this.body.Depn_DOB = date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear();
    }
    else{
      alert("Invalid Date")
    }
  },
    err => console.log('Error occurred while getting date: ', err)
  );
}
}
uploadFile(){
  let fileUrl="http://103.1.82.29:807/api/UserActivity/UploadDocumentFile"
  this.filechooser.open().then((uri)=>{
    this.filepath.resolveNativePath(uri).then((nativepath)=>{
      let files = nativepath 
      this.fileName   = nativepath.substring(nativepath.lastIndexOf("/") + 1);
      this.fileType   = this.fileName.substring(this.fileName.lastIndexOf(".") + 1);
      if(this.fileType=="pdf"||this.fileType=="jpg"){
      alert("Selected "+this.fileName)
      let body={
        "TestFile": this.fileName
      }
      let formData:FormData=new FormData()
      for(var key in body){
        console.log(key,body[key])
        formData.append(key,body[key])
      }
      this.auth.uploadDoc(body).subscribe(res=>{
        if(res.Status==1){
          alert("File Uploaded")
        }
       else{
         alert(res.Message)
       }
      },
      err=>{
        alert(JSON.stringify(err))
      })
    }
    else{
      alert("Only pdf or jpg file can be selected")
      this.fileName=""
    }
    },err=>{
     // alert(err)
    })
  })
}
submitBasic(){
  if(this.validate()){
  //  if(this.validatePan()){
  let body={
    "perno":this.body.personal_no,                
 "rank_cd": this.body.rank_desc,
 "ppo_no" : this.body.PPO_No,
 "correg_ppo_no": this.body.corr_ppo,
 "aadhar_no": this.body.Self_Aadhar,
 "pan_no": this.body.self_PAN,
 "Emil_id_slef": this.body.Self_Email,
 "mobile_no_self": this.body.Self_Mobile,
 "nqsmth":  this.body.NQSMTH,
 "nqsdays": this.body.NQSDAYS,
 
 "nok_name": this.body.NOK_Name,                
 "spouse_dob":this.body.NOK_DOB,
 "spouse_aadhar" : this.body.NOK_Aadhar,
 "spouse_PAN": this.body.NOK_PAN,
 "Email_id": this.body.NOK_Email,
 "mobile_no": this.body.NOK_Mobile1,
 
 "dep_name": this.body.Depn_Name,                
 "dep_dob": this.body.Depn_DOB,
 "dep_aadhar" : this.body.Depn_Aadhar,
 "dep_email": this.body.Depn_Email,
 "dep_mob": this.body.Depn_Mobile,
 "dep_rel_cd": this.body.Relation,
 "dep_fp_type": this.body.Pensioner_Type,
 
 "bsr_code": this.body.BSR_Code_Link_br,
 "link_br_add" : this.body.Link_Br_Add,
 "pension_acct":this.body.Pensioner_Acct_No,
 "IFSC_code": this.body.IFSC_Code,
 "bsrcode_paying_bank": this.body.BSRCode_paying_Bank,
 "pension_paying_br_add":this.body.pension_paying_br_add,
 "Pension_paying_city": this.body.City_name_paying_bank,
 "pin_code1": this.body.pin_code,
 "cdaacctno": this.body.CDAACCTNO
 }
 console.log(JSON.stringify(body))
 this.auth.editLpcProfile(body).subscribe(res=>{
  console.log(JSON.stringify(res))
  if(res.Status==1){
  alert("LPC Profile Updated")
  //this.presentActionSheet()
  this.router.navigate(['/uploadDoc'])
  
  }
 else if(res.Status==0){
    alert("LPC Profile Not Updated")
    //this.presentActionSheet()
    }
})
//}
}
}
validateMail(){
  if(this.body.NOK_Email!=null||this.body.Depn_Email!=null){
  let regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
  if(this.body.NOK_Email.match(regex)||this.body.Depn_Email.match(regex))
{
return true;
}
else
{
alert("You have entered an invalid email  id!");
return false;
}
  }
}
validatePan(){
  let regex= "^[A-Z0-9]*$"
    if( this.body.NOK_PAN.match(regex))
    {
      return true;
    }
    else
    {
      alert("Space and Special Charcters not allowed in PAN No.");
      return false;
      
    }
}
validate(){
 if(this.body.BSR_Code_Link_br==undefined||this.body.BSR_Code_Link_br==null){
alert("Please Enter BSR Code of Link Branch")
 }
 else if(this.body.Link_Br_Add==undefined||this.body.Link_Br_Add==null){
  alert("Please Enter Link Branch with address")
 }
 else if(this.body.Pensioner_Acct_No==undefined||this.body.Pensioner_Acct_No==null){
  alert("Please Enter Pensioner Bank A/C No") 
}
else if(this.body.IFSC_Code==undefined||this.body.IFSC_Code==null){
  alert("Please Enter IFSC Code") 
}
else if(this.body.BSRCode_paying_Bank==undefined||this.body.BSRCode_paying_Bank==null){
  alert("Please Enter bsr code of pension paying bank")
}
else if(this.body.pension_paying_br_add==undefined||this.body.pension_paying_br_add==null){
  alert("Please Enter Address of Branch paying Pension")
}
else if(this.body.City_name_paying_bank==undefined||this.body.City_name_paying_bank==null){
  alert("Please Enter Pension paying Bank City")
}
else if(this.body.pin_code==undefined||this.body.pin_code==null){
  alert("Please Enter Pin Code")
}
else if(this.body.CDAACCTNO==undefined||this.body.CDAACCTNO==null){
  alert("Please Enter CDA Account Number") 
}
else{
  return true;
}
}
async presentActionSheet() {
 
  const actionSheet = await this.actionSheetController.create({
    header: 'Albums',
    buttons: [{
      text: 'Edit Basic Details',
      role: 'destructive',
      icon: 'create-outline',
      handler: () => {
        this.editBasic();
        console.log('Delete clicked');
      }
    },
    {
      text: 'Print LPC Form',
      icon: 'print-outline',
      handler: () => {
        this.invoiceOpen();
        console.log('Play clicked');
      }
    }, {
      text: 'Upload LPC',
      icon: 'cloud-upload-outline',
      handler: () => {
        this.uploadDoc();
        console.log('Favorite clicked');
      }
    },
    {
      text: 'Upload Misc. Documents',
      icon: 'cloud-upload-outline',
      handler: () => {
        this.uploadDoc();
        console.log('Play clicked');
      }
    },
     {
      text: 'Cancel',
      icon: 'close',
      role: 'cancel',
      handler: () => {
        console.log('Cancel clicked');
      }
    }]
  });
  await actionSheet.present();
}

async showMenu(){
  const popover = await this.popoverController.create({
    component:PersonalMenuPopComponent,
    cssClass: 'popInfo',
    //event: ev,
    translucent: true
  });
  await popover.present();
}
logout(){
  alert("Successfully Sign Out")
    localStorage.clear();
    this.router.navigate(['/home'])
}
}
