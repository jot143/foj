import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { File } from '@ionic-native/file/ngx';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { ModalController, PopoverController } from '@ionic/angular';
import { AuthService } from 'src/app/auth.service';
import { PersonalMenuPopComponent } from '../personal-menu-pop/personal-menu-pop.component';

@Component({
  selector: 'app-editlpc',
  templateUrl: './editlpc.component.html',
  styleUrls: ['./editlpc.component.scss'],
})
export class EditlpcComponent implements OnInit {
  persno: string;
  uploadText: string;
  data: any;
  fileTransfer:FileTransferObject;
  fileName="";
  fileType: string;
  constructor(private modalContrller: ModalController,private auth:AuthService,private router:Router,private popoverController:PopoverController,
    
    private transfer: FileTransfer,
    private file: File,
    private filepath: FilePath,
    private filechooser: FileChooser) {
      this.data=this.auth.userData
    console.log(this.data)
    this.uploadText='';
    // this.spouse_aadhar=this.editData.spouse_aadhar;
   }

   @Input() editData;
   cda='ash n s';
   bsrcode;accountno;ifsccode;payingaddress;payingcity;pincode;bankaddress;bsrcodepaying;
   spouse_aadhar;spouse_dob;spousename;spouseemail;spousemobile;spouserelation;spousetype;spouse_PAN;
   
    ngOnInit() {
      console.log(this.editData);
      // this.spouse_aadhar=this.editData[0].spouse_aadhar;
      // this.spouse_dob=this.editData[0].spouse_dob;
      // this.spouse_PAN=this.editData[0].spouse_PAN;
      // this.spouseemail=this.editData[0].Email_id;
      // this.spousemobile=this.editData[0].mobile_no;
     
      // this.bsrcode=this.editData[0].bsr_code;
      // this.accountno=this.editData[0].cdaacctno;
      // this.ifsccode=this.editData[0].IFSC_code;
      // this.payingaddress=this.editData[0].pension_paying_br_add;
      // this.payingcity=this.editData[0].Pension_paying_city;
      // this.pincode=this.editData[0].pin_code1;
      // this.bsrcodepaying=this.editData[0].bsrcode_paying_bank;
    }
   
    submit() {
let body={
   "perno":this.data.personal_no,                
"rank_cd": this.data.rank_desc,
"ppo_no" : this.data.PPO_No,
"correg_ppo_no": this.data.corr_ppo,
"aadhar_no": this.data.Self_Aadhar,
"pan_no": this.data.self_PAN,
"Emil_id_slef": this.data.Self_Email,
"mobile_no_self": this.data.Self_Mobile,
"nqsmth":  this.data.NQSMTH,
"nqsdays": this.data.NQSDAYS,

"nok_name": this.data.NOK_Name,                
"spouse_dob":this.data.NOK_DOB,
"spouse_aadhar" : this.data.NOK_Aadhar,
"spouse_PAN": this.data.NOK_PAN,
"Email_id": this.data.NOK_Email,
"mobile_no": this.data.NOK_Mobile1,

"dep_name": this.data.Depn_Name,                
"dep_dob": this.data.Depn_DOB,
"dep_aadhar" : this.data.Depn_Aadhar,
"dep_email": this.data.Depn_Email,
"dep_mob": this.data.Depn_Mobile,
"dep_rel_cd": this.data.Relation,
"dep_fp_type": this.data.Pensioner_Type,

"bsr_code": this.data.BSR_Code_Link_br,
"link_br_add" : this.data.Link_Br_Add,
"pension_acct":this.data.Pensioner_Acct_No,
"IFSC_code": this.data.IFSC_Code,
"bsrcode_paying_bank": this.data.BSRCode_paying_Bank,
"pension_paying_br_add":this.data.pension_paying_br_add,
"Pension_paying_city": this.data.City_name_paying_bank,
"pin_code1": this.data.pin_code,
"cdaacctno": this.data.CDAACCTNO
}


// let body={

//   "perno": this.data.personal_no,              
//   "House_no": this.data.houseNo,
//   "Moh_st" : this.data.mohSt,
//   "Village": this.data.village,
//   "Tehsil": this.data.tehsil ,
//   "Post_office": this.data.postOffice,
//   "Telegraph_office": this.data.telegraphOffice,
//   "Police_stn": this.data.policeStation,
//   "State_cd": this.data.stateDesc,
// "district_cd": this.data.districtDesc,
// "Pin_Code": this.data.pinCode,

// "ppo_no": this.data.ppoNo,                
//   "disab": this.data.disabStatus,
//   "comp_dis_percent" : this.data.disabPercent,
//   "pan_no": this.data.selfPan,
//   "aadhar_no": this.data.selfAadhar,
//   "entry_type_cd": this.data.selfAadhar,
// "course_no": this.data.courseSerNo,                
//   "mobile_no_self": this.data.selfMobile,
//   "alt_mobile_no" : this.data.selfAltMobile,
//   "Emil_id_slef": this.data.selfEmail,
//   "Corp_cd": this.data.corpsName,
//   "penid": this.data.penId,
// "Gal_Amt":this.data.gal,


// "nok_name": this.data.NOK_Name,                
// "spouse_dob":this.data.NOK_DOB,
// "spouse_aadhar" : this.data.NOK_Aadhar,
// "spouse_PAN": this.data.NOK_PAN,
// }
this.auth.editLpcProfile(body).subscribe(res=>{
  console.log(JSON.stringify(res))
  if(res.Status==1){
  alert("LPC Profile Updated")
  this.getData()
  }
})
       
      console.log(this.editData)
      // console.log(contactForm.value.persno);
      // console.log(contactForm.value.rank_cd);
      console.log(this.spouse_aadhar)
    }
    getData(){
      this.persno=localStorage.getItem('personalNo')
      let body={
        "personalNo": this.persno
    }
      this.auth.getUserInfo(body).subscribe(res=>{
       this.auth.userData=res[0];
       this.router.navigate(['/openlpc'])
      console.log(JSON.stringify(this.auth.userData))
      })
    }

    home(){
      this.modalContrller.dismiss();
    }
    uploadFile(){
      this.filechooser.open().then((uri)=>{
        this.filepath.resolveNativePath(uri).then((nativepath)=>{
          let files = nativepath 
          this.fileName   = nativepath.substring(nativepath.lastIndexOf("/") + 1);
          this.fileType   = this.fileName.substring(this.fileName.lastIndexOf(".") + 1);
          if(this.fileType=="pdf"||this.fileType=="jpg"){
          this.fileTransfer=this.transfer.create();
          let options:FileUploadOptions={
         fileKey:'pdffile',
         fileName:"doc.pdf",
         chunkedMode:false,
         headers:{},
         mimeType:'doc/pdf'
          }
          this.uploadText="uploading..";
          this.fileTransfer.upload(nativepath,'api url',options).then((data)=>{
            alert('done'+ JSON.stringify(data));
            this.uploadText="";
          },err=>{
            alert(err);
          })
        }
          else{
           alert("Only pdf or jpg file can be selected")
           this.fileName=""
          }
        },err=>{
          alert(err)
        })
      })
    }
    async showMenu(){
      const popover = await this.popoverController.create({
        component:PersonalMenuPopComponent,
        cssClass: 'popInfo',
        //event: ev,
        translucent: true
      });
      await popover.present();
    }
    logout(){
      alert("Successfully Sign Out")
    localStorage.clear();
    this.router.navigate(['/home'])
    }
}
