import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth.service';
import { File } from '@ionic-native/file/ngx';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { PopoverController } from '@ionic/angular';
import { PersonalMenuPopComponent } from '../personal-menu-pop/personal-menu-pop.component';
@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.component.html',
  styleUrls: ['./editprofile.component.scss'],
})
export class EditprofileComponent implements OnInit {
data;
  uploadText: string;
  fileTransfer:FileTransferObject;
  persno: string;
  fileName="";
  fileType: string;
  constructor(private auth:AuthService,private router:Router,private popoverController:PopoverController,
   
    private transfer: FileTransfer,
    private file: File,
    private filepath: FilePath,
    private filechooser: FileChooser) {
    this.data=this.auth.userData
    this.uploadText='';
   }

  ngOnInit() {}
  submit(){
    let personalNo=localStorage.getItem('personalNo')
    let spouseDob=this.data.spouseDob.substring(6, 10)+"-"+this.data.spouseDob.substring(3, 5)+"-"+this.data.spouseDob.substring(0, 2)
    let body={

      "perno": personalNo,                
      "House_no": this.data.houseNo,
      "Moh_st" : this.data.mohSt,
      "Village": this.data.village,
      "Tehsil": this.data.tehsil ,
      "Post_office": this.data.postOffice,
      "Telegraph_office": this.data.telegraphOffice,
      "Police_stn": this.data.policeStation,
      "State_cd": this.data.stateDesc,
"district_cd": this.data.districtDesc,
"Pin_Code": this.data.pinCode,

"ppo_no": this.data.ppoNo,                
      "disab": this.data.disabStatus,
      "comp_dis_percent" : this.data.disabPercent,
      "pan_no": this.data.selfPan,
      "aadhar_no": this.data.selfAadhar,
      "entry_type_cd": this.data.selfAadhar,
"course_no": this.data.courseSerNo,                
      "mobile_no_self": this.data.selfMobile,
      "alt_mobile_no" : this.data.selfAltMobile,
      "Emil_id_slef": this.data.selfEmail,
      "Corp_cd": this.data.corpsName,
      "penid": this.data.penId,
"Gal_Amt":this.data.gal,

"nok_name": this.data.spouseName,                
      "spouse_dob": spouseDob+"T00:00:00",
      "spouse_aadhar" : this.data.spouseAadhar,
      "spouse_PAN": this.data.spousePanNo
    }



    
    console.log(JSON.stringify(body))
    this.auth.editProfile(body).subscribe(res=>{
      console.log(JSON.stringify(res))
      if(res.Status==1){
      alert("Profile Updated")
      this.getData()
      }
    })
  }
  updateEmailMobile(){
    this.router.navigate(['/updateMail'])
    
  }
  getData(){
    this.persno=localStorage.getItem('personalNo')
    let body={
      "personalNo": this.persno
  }
    this.auth.getUserInfo(body).subscribe(res=>{
     this.auth.userData=res[0];
     this.router.navigate(['/personalInfo'])
    console.log(JSON.stringify(this.auth.userData))
    })
  }
  uploadFile(){
    this.filechooser.open().then((uri)=>{
      this.filepath.resolveNativePath(uri).then((nativepath)=>{
        let files = nativepath 
      this.fileName   = nativepath.substring(nativepath.lastIndexOf("/") + 1);
      this.fileType   = this.fileName.substring(this.fileName.lastIndexOf(".") + 1);
      if(this.fileType=="pdf"||this.fileType=="jpg"){
        this.fileTransfer=this.transfer.create();
        let options:FileUploadOptions={
       fileKey:'pdffile',
       fileName:"doc.pdf",
       chunkedMode:false,
       headers:{},
       mimeType:'doc/pdf'
        }
        this.uploadText="uploading..";
        this.fileTransfer.upload(nativepath,'api url',options).then((data)=>{
         // alert('done'+ JSON.stringify(data));
          this.uploadText="";
        },err=>{
          //alert(err);
        })
      }
        else{
         alert("Only pdf or jpg file can be selected")
         this.fileName=""
        }
      },err=>{
        //alert(err)
      })
    })
  }
  async showMenu(){
    const popover = await this.popoverController.create({
      component:PersonalMenuPopComponent,
      cssClass: 'popInfo',
      //event: ev,
      translucent: true
    });
    await popover.present();
  }
  logout(){
    alert("Successfully Sign Out")
    localStorage.clear();
    this.router.navigate(['/home'])
  }
}
