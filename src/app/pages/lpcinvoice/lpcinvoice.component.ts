import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { PDFGenerator } from '@ionic-native/pdf-generator/ngx';
import { Router } from '@angular/router';
@Component({
  selector: 'app-lpcinvoice',
  templateUrl: './lpcinvoice.component.html',
  styleUrls: ['./lpcinvoice.component.scss'],
})
export class LpcinvoiceComponent implements OnInit {
  @Input() editData;
  content: string;
  constructor(private modalContrller: ModalController, 
    private pdfGenerator: PDFGenerator,private modalCtrl: ModalController,private router:Router,) { }

  ngOnInit() {}
 
 

  downloadInvoice() {
    this.content = document.getElementById('PrintInvoice').innerHTML;
    let options = {
      documentSize: 'A4',
      type: 'share',
      // landscape: 'portrait',
      fileName: 'Order-Invoice.pdf'
    };
    this.pdfGenerator.fromData(this.content, options)
      .then((base64) => {
        console.log('OK', base64);
      }).catch((error) => {
        console.log('error', error);
      });

  }
  dismiss(){
    this.modalCtrl.dismiss();
    this.router.navigate(['/basiclpc'])
      }
}
