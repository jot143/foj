import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PopoverController } from '@ionic/angular';
import { AuthService } from 'src/app/auth.service';
import { PersonalMenuPopComponent } from '../personal-menu-pop/personal-menu-pop.component';

@Component({
  selector: 'app-updateaddress',
  templateUrl: './updateaddress.component.html',
  styleUrls: ['./updateaddress.component.scss'],
})
export class UpdateaddressComponent implements OnInit {
  states: any;
  district: any;
  selectedState: any;
  selectedDistrict: any;
  houseNo: any;
  mohalla: any;
  village: any;
  tehsil: any;
  postOffice: any;
  policeStation: any;
  pinCode: any;
  mobile: any;
  
  constructor(private auth:AuthService,private router:Router,private popoverController:PopoverController) { }

  ngOnInit() {
this.getState()
  }
getState(){
  this.auth.getState().subscribe(res=>{
   this.states=res;
  })
}
getDistrict(ev){
  let val=ev.target.value;
  this.selectedState=val
  this.auth.getDistrict(val).subscribe(res=>{
    this.district=res;
    console.log(JSON.stringify(this.district))
   })
 
}
getDistrictValue(event){
  this.selectedDistrict=event.target.value;
}
updateAddress(){
  let personalNo=localStorage.getItem('personalNo')
  if(this.validateNo()){
 let body= {
  "Personal_no": personalNo,
    "House_no": this.houseNo,
        "Moh_st": this.mohalla,
        "Village": this.village,
       // "Tehsil": this.tehsil,
        "Post_office": this.postOffice,
        "Police_stn": this.policeStation,
        "District_cd": this.selectedDistrict,
        "State_cd": this.selectedState,
        "Pin_code": this.pinCode,
        "Mobile_no": this.mobile
}
  this.auth.updateAddress(body).subscribe(res=>{
     if(res.Status==1){
       alert("Updated Successfully");
       this.getData();
       //this.router.navigate(['/personalInfo'])
     }
    else if(res.Status==0){
      alert("Please Fill All Fields");
      //this.getData();
      //this.router.navigate(['/personalInfo'])
    }
  })
}
  
}

validateNo(){
   if(this.mobile!=undefined||this.mobile!=''){
  let regex = /[0-9]+/g
  if( this.mobile.match(regex))
  {
    if(this.mobile.length<10){
      alert("Enter 10 digit mobile no.")
      return false;
    }
  //alert("Enter Valid Mobile No!");
  return true;
  }
  else
  {
  alert("You have entered an invalid mobile no!");
  return false;
  }
}
}

getData(){
  let persno=localStorage.getItem('personalNo')
  let body={
    "personalNo": persno
}
  this.auth.getUserInfo(body).subscribe(res=>{
   this.auth.userData=res[0];
   this.router.navigate(['/personalInfo'])
  
  })
}
async showMenu(){
  const popover = await this.popoverController.create({
    component:PersonalMenuPopComponent,
    cssClass: 'popInfo',
    //event: ev,
    translucent: true
  });
  await popover.present();
}
logout(){
  alert("Successfully Sign Out")
    localStorage.clear();
    this.router.navigate(['/home'])
}

}
