import { Component, OnInit } from '@angular/core';
import { ModalController, PopoverController } from '@ionic/angular';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { Router } from '@angular/router';
import { PersonalMenuPopComponent } from '../personal-menu-pop/personal-menu-pop.component';
import { AuthService } from 'src/app/auth.service';
import { LpcinvoiceComponent } from '../lpcinvoice/lpcinvoice.component';
@Component({
  selector: 'app-uploaddata',
  templateUrl: './uploaddata.component.html',
  styleUrls: ['./uploaddata.component.scss'],
})
export class UploaddataComponent implements OnInit {
  uploadText:any;
  downloadText:any;
  fileTransfer:FileTransferObject;
  fileName: string;
  fileType: string;
  filePrefix: string;
  constructor(private modalCtrl: ModalController,private router:Router,
    private transfer: FileTransfer,
    private file: File,
    private filepath: FilePath,
    private filechooser: FileChooser,
    private popoverController:PopoverController,private auth:AuthService) {
      this.uploadText='';
      this.downloadText="";
     }

  ngOnInit() {}
  selectFile(event){
   // this.file = event.target.files[0]
    console.log(event.target.files[0])
    this.filechooser.open().then((uri)=>{
      this.filepath.resolveNativePath(uri).then((nativepath)=>{
        let files = nativepath 
        
        console.log(this.file)
        this.fileName   = nativepath.substring(nativepath.lastIndexOf("/") + 1);
        this.fileType   = this.fileName.substring(this.fileName.lastIndexOf(".") + 1);
        this.filePrefix=this.fileName.substring(0,this.fileName.lastIndexOf("."));
        if(this.fileType=="pdf"||this.fileType=="jpg"){
          this.file = event.target.files[0];
        }
      else{
        alert("Only pdf or jpg file can be selected")
        this.fileName=""
      }
      },err=>{
       // alert(err)
      })
    })
  
  }

  async invoiceOpen(){
    const modal = await this.modalCtrl.create({
      component: LpcinvoiceComponent,
      componentProps: {
      editData:this.auth.lpcDetails
      }
  
    });
    return await modal.present();
  }

  uploadFile(){
    let personal=localStorage.getItem('personalNo')
    let body={
      "TestFile": this.file,
      "FilePrefix":personal
    }
    let formData:FormData=new FormData()
    for(var key in body){
      console.log(key,body[key])
      formData.append(key,body[key])
    }
    //formData.append("file", this.file, file.name);
    this.auth.uploadDoc(formData).subscribe(res=>{
      if(res.Status==1){
        alert("File Uploaded")
      }
     else{
       alert(res.Message)
     }
    },
    err=>{
      alert(JSON.stringify(err))
    })
    //  this.filechooser.open().then((uri)=>{
    //   this.filepath.resolveNativePath(uri).then((nativepath)=>{
    //     let files = nativepath 
    //     this.fileName   = nativepath.substring(nativepath.lastIndexOf("/") + 1);
    //     this.fileType   = this.fileName.substring(this.fileName.lastIndexOf(".") + 1);
    //     this.filePrefix=this.fileName.substring(0,this.fileName.lastIndexOf("."))
    //     if(this.fileType=="pdf"||this.fileType=="jpg"){
    //     alert("Selected "+this.fileName)
    //     let body={
    //       "TestFile": this.fileName,
    //       "FilePrefix":this.filePrefix
    //     }
    //     let formData:FormData=new FormData()
    //     for(var key in body){
    //       console.log(key,body[key])
    //       formData.append(key,body[key])
    //     }
    //     this.auth.uploadDoc(formData).subscribe(res=>{
    //       if(res.Status==1){
    //         alert("File Uploaded")
    //       }
    //      else{
    //        alert(res.Message)
    //      }
    //     },
    //     err=>{
    //       alert(JSON.stringify(err))
    //     })
    //   }
    //   else{
    //     alert("Only pdf or jpg file can be selected")
    //     this.fileName=""
    //   }
    //   },err=>{
    //    // alert(err)
    //   })
    // })
  }
  
  downloadFile(){
    this.downloadText="downloading...";
    this.fileTransfer.download('file url',this.file.externalRootDirectory+ "rodra.pdf").then((data)=>{
      alert('completed..');
      this.downloadText="";
    },err=>{
      this.downloadText="";
      alert('file downloading failed..');
    })
  }
  
    dismiss(){
  this.modalCtrl.dismiss();
  this.router.navigate(['/basiclpc'])
    }

    async showMenu(){
      const popover = await this.popoverController.create({
        component:PersonalMenuPopComponent,
        cssClass: 'popInfo',
        //event: ev,
        translucent: true
      });
      await popover.present();
    }
    logout(){
      alert("Successfully Sign Out")
        localStorage.clear();
        this.router.navigate(['/home'])
    }

    gotoLPC(){
      this.router.navigate(['/openlpc'])
    }
}
