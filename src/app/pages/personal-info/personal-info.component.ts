import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PopoverController } from '@ionic/angular';
import { AuthService } from 'src/app/auth.service';
import { PersonalMenuPopComponent } from '../personal-menu-pop/personal-menu-pop.component';
 
@Component({
  selector: 'app-personal-info',
  templateUrl: './personal-info.component.html',
  styleUrls: ['./personal-info.component.scss'],
})
export class PersonalInfoComponent implements OnInit {
data;
userNo
  constructor(private router:Router,private route: ActivatedRoute,private popoverController:PopoverController,private auth:AuthService) {
   
    //this.userNo=this.auth.userProfile.PersonalNo
    this.data=this.auth.userData;

   }

  ngOnInit() {
    
   // this.getData()
  }
 ionViewWillEnter(){
  this.data=this.auth.userData;
  console.log("fghhj")
  console.log(JSON.stringify(this.auth.userData))
 }
 async showMenu(){
    const popover = await this.popoverController.create({
      component:PersonalMenuPopComponent,
      cssClass: 'popInfo',
      //event: ev,
      translucent: true
    });
    await popover.present();
  }
  logout(){
    alert("Successfully Sign Out")
    localStorage.clear();
    this.router.navigate(['/home'])
  }
}
