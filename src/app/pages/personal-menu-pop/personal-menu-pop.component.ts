import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, PopoverController } from '@ionic/angular';
import { AuthService } from 'src/app/auth.service';
import { EditprofileComponent } from '../editprofile/editprofile.component';

@Component({
  selector: 'app-personal-menu-pop',
  templateUrl: './personal-menu-pop.component.html',
  styleUrls: ['./personal-menu-pop.component.scss'],
})
export class PersonalMenuPopComponent implements OnInit {
  mpinCheck: string;

  constructor(private router:Router,private popover:PopoverController,private modalCtrl: ModalController,private auth:AuthService) { }

  ngOnInit() {}
 async goToMenu(i){
    if(i==1){
      this.router.navigate(['/editProfile'])
    await this.popover.dismiss();
      
    }
   else if(i==2){
    this.router.navigate(['/changePass'])
    await this.popover.dismiss();
    }
    else if(i==12){
      this.router.navigate(['/changemPin'])
      await this.popover.dismiss();
      }
    else if(i==3){
      this.router.navigate(['/grievance'])
      await this.popover.dismiss();
      }
      else if(i==4){
        // window.open(
        //   'https://rodra.gov.in/Download.aspx',
        //   "_self",
        //   "hardwareback : yes,hideurlbar:yes"
        // );
        this.router.navigate(['/downloadsecond'])
        await this.popover.dismiss();
        }
        else if(i==5){
          //this.router.navigate(['/grievance'])
          await this.popover.dismiss();
          }
   else if(i==6){
    this.router.navigate(['/book'])
    await this.popover.dismiss();
    }
    else if(i==7){
      this.router.navigate(['/feedback'])
      await this.popover.dismiss();
      }
    else if(i==8){
      this.router.navigate(['/updateaddress'])
      await this.popover.dismiss();
      }
      else if(i==9){
        this.router.navigate(['/dakStatus'])
        await this.popover.dismiss();
        }
        else if(i==10){
          this.getLpcDetails();
          
          await this.popover.dismiss();
          }
          else if(i==11){
            let personalNo=localStorage.getItem('personalNo')
            let body={
              "Pin" :personalNo
          }
            this.auth.viewnems(body).subscribe(async res=>{
              this.auth.nemsInfo=res[0]
              this.router.navigate(['/viewnems'])
            await this.popover.dismiss();
        
            })
            
            }
            else if(i==13){
              this.router.navigate(['/scheduleList'])
              await this.popover.dismiss();
              }
              else if(i==14){
                this.checkForMpin();
               // this.router.navigate(['/creatempin'])
                await this.popover.dismiss();
                }
            
  }
  checkForMpin(){
    this.mpinCheck=localStorage.getItem('mpin')
    if(this.mpinCheck!=undefined||this.mpinCheck!=""){
      alert("Your MPIN is already created .If You want to change MPIN then goto Change MPIN page")
      this.router.navigate(['/changemPin'])
    }
    else{
      this.router.navigate(['/creatempin'])
    }
  }
  getLpcDetails(){
    let personalNo=localStorage.getItem('personalNo')
    let body={
      "perno" : personalNo
    }
    this.auth.getLpcDetails(body).subscribe(res=>{
      console.log(JSON.stringify(res))
      this.router.navigate(['/openlpc'])
      this.auth.lpcDetails=res[0]
    }
    )
    
  }
  
}
