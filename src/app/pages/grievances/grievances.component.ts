import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import { File } from '@ionic-native/file/ngx';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { PopoverController } from '@ionic/angular';
import { AuthService } from 'src/app/auth.service';
import { PersonalMenuPopComponent } from '../personal-menu-pop/personal-menu-pop.component';
import { SplashComponent } from '../splash/splash.component';

@Component({
  selector: 'app-grievances',
  templateUrl: './grievances.component.html',
  styleUrls: ['./grievances.component.scss'],
})
export class GrievancesComponent implements OnInit {
  grievanceList: any;
  date2: string;
  date1: string;
  date: string;
  today: string;
  griev: any;
  grievanceType: any;
  ref1: string;
  ref2: string;
  ref1date: string;
  ref2date: string;
  grievanceStatusList: any;
  uploadText: string;
  fileTransfer:FileTransferObject;
  upload=false;
  formData: FormData;
  fileName: string;
  fileType: string;
  filePrefix: string;
  //file: any;
  constructor(private popoverController:PopoverController,private auth:AuthService,private datePicker: DatePicker,
    private router:Router,
    private transfer: FileTransfer,
    private file: File,
    private filepath: FilePath,
    private filechooser: FileChooser) {
      this.uploadText='';
     }
   new=true;
   persno
   tDay = new Date();
  ngOnInit() {
    this.fileName=""
    this.ref1=""
    this.ref2=""   
   this.ref1date="",
    this.ref2date="",
    this.persno=localStorage.getItem('personalNo')
   let date= new Date()
  this.today= date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate();
    this.getGrievanceList();
    this.getGrievanceStatus();
  }
  getUserName(){
    this.persno=this.persno.toUpperCase()
  }
  segmentChanged(ev){
  let val=ev.target.value;
  if(val=="new"){
   this.new=true;
  }
  else if(val=="status"){
   this.new=false
  }
  }
  getGrievanceList(){
    this.auth.getGrievanceList().subscribe(res=>{
      this.grievanceList=res;

    })
  }
 async trackCase(caseId){
  let personalNo=localStorage.getItem('personalNo')
  
  
  let body= { 
    "Personal_no" :personalNo,
    "Case_Id" : caseId
}
  this.auth.trackCaseWithId(body).subscribe(async res=>{
    this.auth.dataOfTrackCaseWithId=res;
    const popover = await this.popoverController.create({
      component: SplashComponent,
      cssClass: 'popClass',
      //event: ev,
      translucent: true
    });
     popover.present();
  })
    
  }
  grievType(ev){
    this.grievanceType=ev.target.value
  }
  showDatepicker(val){
   if(val==2){
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK,
      okText:"Save Date",
      todayText:"Set Today"
    }).then(
      date => {
        let toCompare=new Date(date.getFullYear(), date.getMonth(), date.getDate())
        if(this.tDay>toCompare){
        this.ref1date=date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate()+"T08:00:00"
        this.date1 = date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear()
        }
        else{
          alert("Reference1 date can't be greater than today date")
        }
      },
      err => console.log('Error occurred while getting date: ', err)
    );
  }
 else if(val==3){
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK,
      okText:"Save Date",
      todayText:"Set Today"
    }).then(
      date => {
        let toCompare=new Date(date.getFullYear(), date.getMonth(), date.getDate())
        if(this.tDay>toCompare){
        this.ref2date= date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate()+"T08:00:00"
        this.date2 = date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear()
        }
        else{
          alert("Reference2 date can't be greater than today date")
        }
      },
      err => console.log('Error occurred while getting date: ', err)
    );
  }
}
submit(){
  this.persno= this.persno.toUpperCase()
  if(this.validation()){
    if(this.validateGriev()){
  let body={
    "pers_no": this.persno,
    "complaint_details":this.griev,
    "comp_subj_cd":this.grievanceType,
    "complaint_dt":this.today+"T00:00:00",
    "RL1":this.ref1,
    "RL2":this.ref2,    
    "RL1_dt":this.ref1date,
    "RL2_dt":this.ref2date,
    "docu_upload_user1" : this.fileName
 }
console.log(JSON.stringify(body))
this.auth.newGrievance(body).subscribe(res=>{
 
if(res.Status==1){
  alert("Successfully Created New Grievance With Id  "+res.Message)
  this.router.navigate(['/personalInfo'])
}
else{
  alert("Grievance Not Submitted")
 this.griev=""
 this.grievanceType=null
}
},err=>{

  console.log(err.error);
})
}
}
}


validateGriev(){
  let regex= "^[a-zA-Z0-9]*$"
   if( this.persno.match(regex))
   {
     return true;
   }
   else
   {
     alert("Space and Special Charcters not allowed");
     return false;
     
   }
 }

validation(){
  if(this.griev==undefined||this.griev==""){
    alert("Please Enter Your Grievance")
  }
  else if(this.grievanceType==undefined||this.grievanceType==null){
    alert("Please Select Your Grievance Subject")
  }
  else{
  return true;
  }
  
}

getGrievanceStatus(){
  let personalNo=localStorage.getItem('personalNo')
  let body={
    "pers_no" :personalNo
}
    this.auth.getGrievanceStatus(body).subscribe(res=>{
      this.grievanceStatusList=res;
    })
}
uploadDoc(ev){
 let type=ev.target.value
 if(type=="yes"){
   this.upload=true;
   
 }
 else{
  this.upload=false;
  this.fileName=""
 }
}
selectFile(event){
  
   console.log(event.target.files[0])
   this.filechooser.open().then((uri)=>{
     this.filepath.resolveNativePath(uri).then((nativepath)=>{
       let files = nativepath 
       
 
       this.fileName   = nativepath.substring(nativepath.lastIndexOf("/") + 1);
       this.fileType   = this.fileName.substring(this.fileName.lastIndexOf(".") + 1);
       this.filePrefix=this.fileName.substring(0,this.fileName.lastIndexOf("."));
       if(this.fileType=="pdf"||this.fileType=="jpg"){
         this.file = event.target.files[0];
       }
     else{
       alert("Only pdf or jpg file can be selected")
       this.fileName=""
     }
     },err=>{
      // alert(err)
     })
   })
 
 }
uploadFile(){ 
  let personal=localStorage.getItem('personalNo')
  let body={
    "TestFile": this.file,
    "FilePrefix":personal
  }
  let formData:FormData=new FormData()
  for(var key in body){
    console.log(key,body[key])
    formData.append(key,body[key])
  }
  //formData.append("file", this.file, file.name);
  this.auth.uploadDoc(formData).subscribe(res=>{
    if(res.Status==1){
      //alert("File Uploaded")
    }
   else{
    // alert(res.Message)
   }
  },
  err=>{
   // alert(JSON.stringify(err))
  })
  //let fileUrl=("http://posttestserver.com/post.php")
  // let fileUrl="http://103.1.82.29:807/Downloads"
  // this.filechooser.open().then((uri)=>{
  //   this.filepath.resolveNativePath(uri).then((nativepath)=>{
  //     let files = nativepath 
  //     this.fileName   = nativepath.substring(nativepath.lastIndexOf("/") + 1);
  //     this.fileType   = this.fileName.substring(this.fileName.lastIndexOf(".") + 1);
  //     //alert(this.fileType)
  //     if(this.fileType=="pdf"||this.fileType=="jpg"){
  //     alert(this.fileName)
  //     this.fileTransfer=this.transfer.create();
  //     let options:FileUploadOptions={
  //    fileKey:'pdffile',
  //    fileName:this.fileName,
  //    chunkedMode:false,
  //    headers:{},
  //    mimeType:'doc/pdf'
  //     }
  //     this.uploadText="uploading..";
  //     this.fileTransfer.upload(nativepath,encodeURI(fileUrl),options).then((data)=>{
  //      // alert('done'+ JSON.stringify(data));
  //      alert("done")
  //       this.uploadText="";
  //     },err=>{
  //      // alert("oops")
  //     //  alert(JSON.stringify(err));
  //     })
  //   }
  //   else{
  //     alert("Only pdf or jpg file can be selected")
  //     this.fileName=""
  //   }
  //   },err=>{
  //    //alert("File not found")
  //    //alert(JSON.stringify(err));
  //   })
  // })
}
async showMenu(){
  const popover = await this.popoverController.create({
    component:PersonalMenuPopComponent,
    cssClass: 'popInfo',
    //event: ev,
    translucent: true
  });
  await popover.present();
}
logout(){
  alert("Successfully Sign Out")
  localStorage.clear();
  this.router.navigate(['/home'])
}
}
