import { TemplateParseResult } from '@angular/compiler';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { LoadingController, NavController } from '@ionic/angular';
import { AuthService } from 'src/app/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  mpinvalue;
  dataFromService: any = "";
  otp = false;
  showpass = true
  otpverified = false;
  otpvalue: string;
  @ViewChild('ngOtpInput', { static: false }) ngOtpInput: any;
  config = {     //otp config..........
    allowNumbersOnly: true,
    length: 4,
    isPasswordInput: false,
    disableAutoFocus: false,
    placeholder: '',
    inputStyles: {
      'width': '50px',
      'height': '50px',
    }
  };
  randomNumber: any;
  randomNumberEntered: any = "";
  isActiveToggleTextPassword: any;
  passtype = "password";
  loading:any;
  constructor(private navCtrl: NavController, private auth: AuthService, private router: Router,private loadingCtrl:LoadingController) { }
  username;
  password;
  show = true;
  dontshow = false;
  profile;
  mPin
  ngOnInit() {
    //this.randomNumber=
    this.generateRandomNumber(1111, 9999);
  }
  generateRandomNumber(min, max) {
    var rand = min + Math.random() * (max - min);
    rand = Math.round(rand);
    this.randomNumber = rand.toString();

  }
  registerIn() {
    this.navCtrl.navigateRoot('/register')
  }
  moveFocus(nextElement){
    nextElement.setFocus();
  }

  
  login() {
    if (this.randomNumber != this.randomNumberEntered) {
      this.randomNumberEntered = "";
      // this.randomNumber=
      this.generateRandomNumber(1111, 9999);
      alert("Enter Correct Captcha")
    }
    else {
      this.username = this.username.toUpperCase()
      if (this.validation()) {
        if (this.validateUserID()) {

          let body = {
            personalNo: this.username,
            password: this.password
          }
          // this.auth.loginAdmin(body).then(res=>{
          //   if(res.Status==1){
          //     localStorage.setItem('token',res.Data.Token)
          //     localStorage.setItem('mpin',res.Data.MPin)
              
          //     this.auth.userProfile=res.Data;
          //     this.presentAlert();
          //     this.getData()

          //   }
          //   else{
          //     this.generateRandomNumber(1111,9999);
          //     alert("User Id or Password is incorrect")
          //   }
          //   }
          //   ,err=>{
          //     console.log("toerr")
          //     console.log(err)
          //   })

          //alert('success..');
          this.auth.loginAdmin(body).subscribe(res=>{
            if(res.Status==1){
              localStorage.setItem('token',res.Token)
              localStorage.setItem('mpin',res.Data[0].MPin)
              this.auth.userProfile=res.Data[0];
              localStorage.setItem('personalNo', this.auth.userProfile.Personal_no)
              this.presentAlert();
              this.getData()

            }
            else{
              this.generateRandomNumber(1111,9999);
              alert("User Id or Password is incorrect")
            }
            }
            ,err=>{
              console.log("toerr")
              alert(JSON.stringify(err))
            })
           // this.auth.getdownloadDoc().subscribe(res=>{
            //   console.log(JSON.stringify(res))
            // },
            // err=>{
            //   console.log(JSON.stringify(err))
            // })
        }
      }
    }
  }

  validation() {
    if (this.username == undefined) {
      alert("Please Enter User ID")
    }
    else if (this.password == undefined) {
      alert("Please Enter Password")
    }

    else {
      return true;
    }
  }

  validateUserID() {

    let regex = "^[A-Z0-9]*$"
    if (this.username.match(regex)) {
      return true;
    }
    else {
      alert("Space and Special Charcters not allowed");
      return false;

    }
  }
  getData() {
    // if(localStorage.getItem('personalNo')==undefined||localStorage.getItem('personalNo')==null){
    // localStorage.setItem('personalNo', this.auth.userProfile.Personal_no)
    // }
    //this.router.navigate(['/personalInfo'])
    let body = {
      "personalNo": localStorage.getItem('personalNo')
    }
    
    this.auth.getUserInfo(body).subscribe(res => {
     this.auth.userData = res[0];
     console.log(JSON.stringify(res[0]))
     
     this.loadingCtrl.dismiss()
     this.router.navigate(['/personalInfo'])
      //console.log(JSON.stringify(this.auth.userData))
    })
  }

 async presentAlert(){
  this.loading = await this.loadingCtrl.create({
    duration:1000,
    message: 'Please wait...'
});
this.loading.present()

  }
  onOtpChange(otpval) {

    this.mpinvalue = otpval;
    console.log(this.mpinvalue.length)
  }
  segmentChanged(ev) {
    if (ev.target.value == "user") {
      this.show = true;
      this.dontshow = false;
    }
    else if (ev.target.value == "mpin") {
      this.show = false;
      this.dontshow = true
    }
  }

  loginWithMpin() {
    let personalNo = localStorage.getItem('personalNo')
    if (this.pinValidation()) {
      let body = {
        "personalNo": personalNo,
        "mPin": this.mpinvalue
      }
      this.auth.loginWithMpin(body).subscribe(res => {
        if (res.Status == 1) {
          this.getData()
          //this.router.navigate(['/personalInfo'])
        }
        else {
          alert("Invalid MPIN")
        }
      })
    }
  }
  pinValidation() {
    if (this.mpinvalue !== undefined) {
      if (this.mpinvalue.length != 4) {
        alert("Please Enter 4 Digit ")
        return false
      }
      else {
        return true
      }
    }
    else {
      alert("Please Enter MPin")
    }
  }

  moveTo(val) {
    if (val == 1) {
      this.router.navigate(["/changePass"], { queryParams: { changeType: 1 } })
    }
    else {
      this.router.navigate(["/changemPin"], { queryParams: { changeType: 2 } })
    }
  }
  toggleTextPassword(val) {

    if (val == 1) {
      this.showpass = false
      //this.passtype="text"
      this.getType(false)
    }
    if (val == 2) {
      this.showpass = true
      this.getType(true)
      //this.passtype="password"
    }
  }

  public getType(bool) {
    this.passtype = bool ? 'password' : 'text';

  }

}
