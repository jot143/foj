import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';  
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { NgOtpInputModule } from  'ng-otp-input';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { SplashComponent } from './pages/splash/splash.component';
import { LoginComponent } from './pages/login/login.component';
import { CreatempinComponent } from './pages/creatempin/creatempin.component';
import { HttpClientModule } from '@angular/common/http';
import { AuthService } from './auth.service';
import { FaqComponent } from './pages/faq/faq.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PopmenuComponent } from './pages/popmenu/popmenu.component';
import { PersonalInfoComponent } from './pages/personal-info/personal-info.component';
import { ExternalLinkComponent } from './pages/external-link/external-link.component';
import { DownloadsComponent } from './pages/downloads/downloads.component';
import { WhatsnewComponent } from './pages/whatsnew/whatsnew.component';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { VideogalleryComponent } from './pages/videogallery/videogallery.component';
import { AboutComponent } from './pages/about/about.component';
import { HTTP } from '@ionic-native/http/ngx';

import { PersonalMenuPopComponent } from './pages/personal-menu-pop/personal-menu-pop.component';
import { ChangePassComponent } from './pages/change-pass/change-pass.component';
import { BookAppointmentComponent } from './pages/book-appointment/book-appointment.component';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import { RegisterComponent } from './pages/register/register.component';
import { StatusComponent } from './pages/status/status.component';
import { GrievancesComponent } from './pages/grievances/grievances.component';
import { DakStatusComponent } from './pages/dak-status/dak-status.component';
import { OpenlpcComponent } from './pages/openlpc/openlpc.component';
import { ViewnemsComponent } from './pages/viewnems/viewnems.component';
import { DownloadsecondComponent } from './pages/downloadsecond/downloadsecond.component';
import { UpdateaddressComponent } from './pages/updateaddress/updateaddress.component';
import { FeedbackComponent } from './pages/feedback/feedback.component';
import { ChangeMpinComponent } from './pages/change-mpin/change-mpin.component';
import { ScheduleListComponent } from './pages/schedule-list/schedule-list.component';
import { ContactusComponent } from './pages/contactus/contactus.component';
import { AskMeListComponent } from './pages/ask-me-list/ask-me-list.component';
import { FeedbackListComponent } from './pages/feedback-list/feedback-list.component';
import { ForgotPassComponent } from './pages/forgot-pass/forgot-pass.component';
import { File } from '@ionic-native/file/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx'
import { PDFGenerator } from '@ionic-native/pdf-generator/ngx';
import { DocumentViewer } from '@ionic-native/document-viewer/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { BasiclpcComponent } from './pages/basiclpc/basiclpc.component';
import { UploaddataComponent } from './pages/uploaddata/uploaddata.component';
import { LpcinvoiceComponent } from './pages/lpcinvoice/lpcinvoice.component';
import { EditlpcComponent } from './pages/editlpc/editlpc.component';
import { EditprofileComponent } from './pages/editprofile/editprofile.component';
import { UpdatemailmobileComponent } from './pages/updatemailmobile/updatemailmobile.component';
import { Ionic4DatepickerModule } from '@logisticinfotech/ionic4-datepicker';
import { FutureVisitComponent } from './pages/future-visit/future-visit.component';
@NgModule({
  declarations: [AppComponent,SplashComponent,
    LoginComponent,
    CreatempinComponent,
    FaqComponent,
    PopmenuComponent,
    PersonalInfoComponent,
    ExternalLinkComponent,
  DownloadsComponent,
  WhatsnewComponent,
  VideogalleryComponent,
  AboutComponent,
  PersonalMenuPopComponent,
  ChangePassComponent,
  BookAppointmentComponent,
  RegisterComponent,
  StatusComponent,
  GrievancesComponent,
  DakStatusComponent,
  OpenlpcComponent,
  ViewnemsComponent,
  DownloadsecondComponent,
  UpdateaddressComponent,
  FeedbackComponent,
  ChangeMpinComponent,
  ScheduleListComponent,
  ContactusComponent,
  AskMeListComponent,
  FeedbackListComponent,
  ForgotPassComponent,
  BasiclpcComponent,
  FutureVisitComponent,
  UploaddataComponent,
  LpcinvoiceComponent,
  EditlpcComponent,
  EditprofileComponent,
  UpdatemailmobileComponent,
  
 
],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule,NgOtpInputModule, HttpClientModule, FormsModule ,ReactiveFormsModule,CommonModule,Ionic4DatepickerModule ],
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy },AuthService,InAppBrowser,HTTP,
    DatePicker,File,FileTransfer,PDFGenerator,DocumentViewer,FileChooser,FilePath,FileOpener,SocialSharing],
  bootstrap: [AppComponent],
})
export class AppModule {}
